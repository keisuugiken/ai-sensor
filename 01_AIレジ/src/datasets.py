import os
import copy
from collections import OrderedDict
import yaml
import xml.etree.ElementTree as ET
import pathlib
import pickle

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

from preprocess import foreground_bbox
from postprocess import show_image, show_bbox


BASE_DIR = os.path.abspath(os.path.dirname(__file__))
TRAIN_IMAGE_NPY = "train.image.npy"
TRAIN_LABEL_PKL = "train.label.pkl"


class Config(OrderedDict):
    def __init__(self, *args, **kwargs):
        self.load_config_yaml()
        super(Config, self).__init__(*args, **self.config)
    
    def load_config_yaml(self, path=os.path.join(BASE_DIR, 'config.yaml')):
        with open(path, "rb") as f:
            self.config = yaml.load(f)

class ElectronicParts(object):
    def __init__(self, config):
        self.C = config

    def load_data(self):
        # if self.C["datasets"]["use_cache"]:
        #     images = self.load_from_npy()
        #     labels = self.load_from_pkl()
        #     if images is not None and labels is not None:
        #         self.images = images
        #         self.labels = labels
        #         return

        data_dirs = self.C["datasets"]["src"]
        all_images = None
        all_labels = []
        all_filenames = []
        for data_dir in data_dirs:
            self.create_dir(data_dir)

            # 画像を読み込む
            image_paths = pathlib.Path(os.path.join(data_dir))
            images = [self.load_image(path) for path in image_paths.glob("*.%s" % self.C["datasets"]["image"]["extension"])]
            images = images
            if all_images is None:
                all_images = np.stack(images)
            else:
                all_images = np.concatenate([all_images, np.stack(images)], axis=0)

            files = list(image_paths.glob("*.%s" % self.C["datasets"]["image"]["extension"]))
            all_filenames += files

            # bboxとラベルを読み込む
            label_paths = pathlib.Path(os.path.join(data_dir))
            labels = [self.load_label(path) for path in label_paths.glob("*.xml")]
            all_labels += labels

        self.images = all_images
        self.labels = all_labels
        self.filenames = all_filenames

        if self.C["datasets"]["use_cache"]:
            self.save_as_npy(images)
            self.save_as_pkl(labels)

    def load_from_npy(self):
        path = os.path.join(self.C["datasets"]["cache_dir"], TRAIN_IMAGE_NPY)
        if os.path.isfile(path):
            return np.load(path)
        else:
            return None
    
    def load_from_pkl(self):
        path = os.path.join(self.C["datasets"]["cache_dir"], TRAIN_LABEL_PKL)
        if os.path.isfile(path):
            with open(path, "rb") as f:
                return pickle.load(f)
        else:
            return None

    def load_image(self, path):
        image = Image.open(path)
        image = image.resize((self.C["datasets"]["image"]["width"], self.C["datasets"]["image"]["height"]))
        image = np.asarray(image)
        return image

    def load_label(self, path):
        with open(path, "rb") as f:
            root = ET.fromstring(f.read())
            labels = [
                [
                    self.xml_get_number(obj, "bndbox", "xmin"),
                    self.xml_get_number(obj, "bndbox", "ymin"),
                    self.xml_get_number(obj, "bndbox", "xmax"),
                    self.xml_get_number(obj, "bndbox", "ymax"),
                    self.xml_get_number(obj, "name"),
                ]
                for obj in root.findall("object")]
            return labels

    def xml_get_node(self, root, *names):
        tree = copy.deepcopy(root)
        for name in names:
            tree = tree.findall(name)[0]
        return tree

    def xml_get_number(self, root, *names):
        node = self.xml_get_node(root, *names)
        return int(node.text)

    def save_as_npy(self, array):
        self.create_dir(self.C["datasets"]["cache_dir"])
        np.save(os.path.join(self.C["datasets"]["cache_dir"], TRAIN_IMAGE_NPY), array)

    def save_as_pkl(self, obj):
        self.create_dir(self.C["datasets"]["cache_dir"])
        with open(os.path.join(self.C["datasets"]["cache_dir"], TRAIN_LABEL_PKL), "wb+") as f:
            pickle.dump(obj, f)

    def create_dir(self, dirpath):
        if not os.path.isdir(dirpath):
            os.makedirs(dirpath)

    def show_image(self, index=0):
        image = self.images[index]
        show_image(image)

    def show_bbox(self, index=0):
        image = self.images[index]
        labels = self.labels[index]
        show_bbox(image, labels)

    def batch_trim_bbox(self):
        dest = self.C["validate"]["dest"]
        # validate用のフォルダに、bboxで切り抜いた画像を保存する
        for i, image in enumerate(self.images):
            labels = self.labels[i]
            filename = str(self.filenames[i])
            target_dir = os.path.dirname(filename.replace("train", "dest"))
            filename = filename.replace(os.path.dirname(filename), "")[1:].replace(".jpg", "")
            if not os.path.exists(target_dir):
                self.create_dir(target_dir)
            for idx, label in enumerate(labels):
                bbox = label[:-1]
                label_index = label[-1]
                bbox_image = foreground_bbox(image, bbox)
                pim = Image.fromarray(bbox_image)
                pim.save(os.path.join(target_dir, filename+"_{}_{}.jpg".format(idx, label_index)))


if __name__ == "__main__":
    C = Config()
    eparts = ElectronicParts(C)
    eparts.load_data()
    print("images : ", eparts.images.shape)
    print("labels : ", len(eparts.labels))
    print("filenames : ", len(eparts.filenames))

    eparts.batch_trim_bbox()
    # eparts.show_image(3)
    # eparts.show_bbox(3)