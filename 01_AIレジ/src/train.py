import os
import datetime

from darkflow.net.build import TFNet
from datasets import Config, ElectronicParts
from preprocess import select_dataset


def _build_model(**options):
    darknet = TFNet(options)
    return darknet

def _main(C=Config()):
    options = C['darknet']
    options.update(C['train']['darknet'])

    # datasets_dir = prepare_train_datasets(C)
    datasets_dir, checkpoint = select_dataset(C)
    options['annotation'] = datasets_dir
    options['dataset']= datasets_dir
    options['backup'] = checkpoint

    options["train"] = True

    model = _build_model(**options)

    print('Enter training ...')
    # try:
    model.train()
    # except KeyboardInterrupt:
    #     print('Rebuild a constant version ...')
    #     model.savepb()


if __name__ == "__main__":
    _main()