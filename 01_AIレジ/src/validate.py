import os
import cv2

from train import _build_model
from datasets import Config
from preprocess import create_checkpoint
from postprocess import save_pb_dir, move_pb


def capture_camera(camera=0, mirror=True, size=None, delegate=lambda frame: frame):
    cap = cv2.VideoCapture(camera)

    while True:
        ret, frame = cap.read()

        if mirror:
            frame = frame[:,::-1]

            if size is not None and len(size) == 2:
                frame = cv2.resize(frame, size)

            if delegate is not None:
                frame = delegate(frame)

            cv2.imshow('camera capture', frame)

            k = cv2.waitKey(1)
            if k == 27: # ESC
                break
    
    cap.release()
    cv2.destroyAllWindows()

def save_pb(C):
    options = C['darknet']
    options.update(C['validate']['darknet'])
    options['savepb'] = True

    pb_dir = save_pb_dir(C)
    options['backup'] = pb_dir.replace('model', 'checkpoints')

    model = _build_model(**options)
    model.savepb()
    move_pb(C)


def _main(C=Config()):
    save_pb(C)

    options = C['darknet']
    options.update(C['validate']['darknet'])

    pb_dir = save_pb_dir(C)
    options['pbLoad'] = os.path.join(pb_dir, 'best.pb')
    options['metaLoad'] = os.path.join(pb_dir, 'best.meta')
    
    model = _build_model(**options)

    def _delegate(frame):
        height, width, channels = frame.shape
        results = model.return_predict(frame)

        # print("==== frame ====")
        for item in results:
            tlx = item['topleft']['x']
            tly = item['topleft']['y']
            brx = item['bottomright']['x']
            bry = item['bottomright']['y']
            label = item['label']
            conf = item['confidence']

            cx = (tlx+brx)/2
            cy = (tly+bry)/2
            # print("x:{0}, y:{1}, label:{2}, conf:{3}".format(cx, cy, label, conf))

            cv2.rectangle(frame, (tlx, tly), (brx, bry), (200,200,0), 2)
            text = label + " " + ('%.2f' % conf)
            cv2.putText(frame,
                text,
                (tlx+10, tly-5),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.6,
                (244, 86, 66),
                2)
        return frame

    capture_camera(size=(C['datasets']['image']['width'], C['datasets']['image']['height']),
                   camera=C['validate']['camera'],
                   delegate=_delegate)


if __name__ == '__main__':
    C = Config()
    # save_pb(C)
    _main(C)