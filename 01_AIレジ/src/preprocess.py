import os
import glob
import shutil
from PIL import Image
import xml.etree.ElementTree as ET
import numpy as np
import copy


# bboxに合わせて画像を切り出す
def foreground_bbox(image, bbox):
    return image[bbox[1]:bbox[3], bbox[0]:bbox[2], :]

# 画像とラベルファイルが正常なセットになっているかチェック
def validate_dataset(image_path, config, debug=False):
    try:
        label_path = image_path.replace("jpg", "xml")
        image = load_image(image_path, config)
        labels = load_label(label_path)
        return label_path
    except Exception as e:
        if debug:
            print("laod error: {}".format(image_path))
            print(e)
        return None

# 画像を一つのフォルダに移動する
def prepare_train_datasets(config):
    from datasets import Config
    config = Config()
    src_paths = config["datasets"]["src"]
    target_dir = os.path.join(config["validate"]["dest"], "datasets")
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    for src in src_paths:
        image_files = glob.glob(os.path.join(src, "*.jpg"))
        for f in image_files:
            filename = os.path.split(f)[-1]
            shutil.copy(f, os.path.join(target_dir, filename))
        label_files = glob.glob(os.path.join(src, "*.xml"))
        for f in label_files:
            filename = os.path.split(f)[-shutil.copy]
            encode_copy(f, os.path.join(target_dir, filename))
    return target_dir


# 画像フォルダを選択する
def select_dataset(config):
    mode = config["train"]["mode"]
    if mode == "all":
        target_dir = load_all_images(config)
    elif mode == "selected":
        target_dir = copy_selected_images(config)
    return target_dir, create_checkpoint(config)

def load_all_images(config):
    d = config["datasets"]["src"]
    root = os.path.join(d, "all")
    mode = ["001", "002", "003"][config["datasets"]["mode"]-1]
    src = os.path.join(root, mode)
    # 画像とラベルをコピー
    target_dir = os.path.join(config["validate"]["dest"], "all", mode)
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    image_files = glob.glob(os.path.join(src, "*.jpg"))
    counter = 0
    for f in image_files:
        filename = os.path.split(f)[-1]
        # 正しいデータセットになっているか
        if validate_dataset(f, config, config["datasets"]["debug"]) is not None:
            # 画像のコピー
            shutil.copy(f, os.path.join(target_dir, filename))
            # ラベルのコピー
            encode_copy(f.replace("jpg", "xml"), os.path.join(target_dir, filename.replace("jpg", "xml")))
            counter += 1
    print("loaded {} images".format(counter))
    return target_dir

# 切り取り画像からデータセットを作成する
def copy_selected_images(config):
    d = config["datasets"]["src"]
    root = os.path.join(d, "selected")
    selected_images = glob.glob(os.path.join(root, "*.jpg"))
    selected_images = [os.path.basename(impath) for impath in selected_images]
    selected_images = [impath.split("_")[0]+".jpg" for impath in selected_images]

    target_dir = os.path.join(config["validate"]["dest"], "selected")
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    
    d = config["datasets"]["src"]
    root = os.path.join(d, "all")
    image_files = glob.glob(os.path.join(root, "**/*.jpg"))
    for im_file in image_files:
        filename = os.path.basename(im_file)
        if filename in selected_images and validate_dataset(im_file, config, config["datasets"]["debug"]) is not None:
            # 画像のコピー
            shutil.copy(im_file, os.path.join(target_dir, filename))
            # ラベルのコピー
            encode_copy(im_file.replace("jpg", "xml"), os.path.join(target_dir, filename.replace("jpg", "xml")))
    return target_dir

# 学習済みチェックポイントフォルダの作成
def create_checkpoint(config):
    d = config["validate"]["dest"]
    train_mode = config["train"]["mode"]
    mode = ["001", "002", "003"][config["datasets"]["mode"]-1]
    target_dir = os.path.join(d, train_mode)
    if train_mode == "all":
        target_dir = os.path.join(target_dir, mode)
    target_dir = os.path.join(target_dir, "checkpoints")
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    return target_dir


def load_image(path, config):
    image = Image.open(path)
    image = image.resize((config["datasets"]["image"]["width"], config["datasets"]["image"]["height"]))
    image = np.asarray(image)
    return image

def xml_get_node(root, *names):
    tree = copy.deepcopy(root)
    for name in names:
        tree = tree.findall(name)[0]
    return tree

def xml_get_number(root, *names):
    node = xml_get_node(root, *names)
    return int(node.text)

def load_label(path):
    root = ET.parse(path)
    labels = [
        [
            xml_get_number(obj, "bndbox", "xmin"),
            xml_get_number(obj, "bndbox", "ymin"),
            xml_get_number(obj, "bndbox", "xmax"),
            xml_get_number(obj, "bndbox", "ymax"),
            xml_get_number(obj, "name"),
        ]
        for obj in root.findall("object")]
    return labels

# bboxから画像を切り出す
def batch_trim_bbox(config):
    d = config["datasets"]["src"]
    root = os.path.join(d, "all")
    paths = glob.glob(os.path.join(root, "**/*.jpg"))
    # すべての画像に対して、ラベル付けされた箇所を切り出してコピーする
    for image_path in paths:
        try:
            label_path = image_path.replace("jpg", "xml")
            image = load_image(image_path, config)
            labels = load_label(label_path)
        except Exception as e:
            print("laod error: {}".format(image_path))
            continue
        for idx, label in enumerate(labels):
            bbox = label[:-1]
            label_index = label[-1]
            bbox_image = foreground_bbox(image, bbox)
            pim = Image.fromarray(bbox_image)
            save_path = image_path.replace("train","dest").replace("all","trim").replace(".jpg", "_{}_{}.jpg".format(idx, label_index))
            target_dir = os.path.dirname(save_path)
            if not os.path.isdir(target_dir):
                os.makedirs(target_dir)
            pim.save(save_path)

def encode_copy(src, dest):
    with open(src, "rb") as f:
        content = f.read()
        content = content.decode("utf-8")
    # try:
    #     content = content.decode("utf-8")
    # except UnicodeDecodeError:
    #     try:
    #         content = content.decode("sjis")
    #     except UnicodeDecodeError as e:
    #         print(src)
    #         raise e
    with open(dest, "w+") as f:
        f.write(content)

if __name__ == "__main__":
    from datasets import Config
    C = Config()
    target, checkpoint = select_dataset(C)
    print(target)
    print(checkpoint)
    # batch_trim_bbox(C)
    # encode_copy("../train/all/01_重なりなし/P1001.xml", "./P1001.xml")