import os
import shutil

import numpy as np
import matplotlib.pyplot as plt


LABEL_COLORS = ["red", "green", "blue"]


def show_image(image):
    plt.imshow(image, interpolation='nearest')
    plt.show()


def show_bbox(image, labels):
    from datasets import Config
    C_ = Config()
    label_names = C_["datasets"]["labels"]

    plt.imshow(image, interpolation='nearest')

    for label in labels:
        bbox = label[:-1]
        name = label_names[label[-1]]

        plt.gca().add_patch(plt.Rectangle(xy=[bbox[0], bbox[1]], width=bbox[2]-bbox[0], height=bbox[3]-bbox[1], fill=False, edgecolor=LABEL_COLORS[label[-1]-1]))
        plt.text(bbox[0]+2, bbox[1]-8, name, fontsize=4, bbox={'facecolor': LABEL_COLORS[label[-1]-1], 'edgecolor': LABEL_COLORS[label[-1]-1], 'alpha': 0.5, 'pad': 1})

    plt.show()

def save_pb_dir(config):
    d = config["validate"]["dest"]
    train_mode = config["train"]["mode"]
    mode = ["001", "002", "003"][config["datasets"]["mode"]-1]
    target_dir = os.path.join(d, train_mode)
    if train_mode == "all":
        target_dir = os.path.join(target_dir, mode)
    target_dir = os.path.join(target_dir, 'model')
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    return target_dir

def move_pb(config):
    pb_dir = save_pb_dir(config)
    base_dir = os.path.abspath(os.path.dirname(__file__))
    shutil.copy(os.path.join(base_dir, 'built_graph', 'dark-yolo.pb'), os.path.join(pb_dir, 'best.pb'))
    shutil.copy(os.path.join(base_dir, 'built_graph', 'dark-yolo.meta'), os.path.join(pb_dir, 'best.meta'))